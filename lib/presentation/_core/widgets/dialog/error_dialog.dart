import 'package:flutter/material.dart';

showErrorDialog({
  required BuildContext context,
  required String message,
  required String title,
  required String buttonText,
  Function? onButtonCLick,
}) {
  showDialog<String>(
    context: context,
    builder: (context) {
      return PopScope(
        canPop: false,
        onPopInvoked: (bool didPop) {
          if (didPop) {
            return;
          }
        },
        child: AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                if (onButtonCLick != null) {
                  onButtonCLick();
                }
              },
              child: Text(buttonText),
            ),
          ],
        ),
      );
    },
  );
}
