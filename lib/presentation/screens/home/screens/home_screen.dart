import 'package:asap_weather/_core/constants/app_constant.dart';
import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/_core/extensions.dart';
import 'package:asap_weather/_core/utils/helpers.dart';
import 'package:asap_weather/presentation/_core/app_color.dart';
import 'package:asap_weather/presentation/_core/app_text_style.dart';
import 'package:asap_weather/presentation/_core/widgets/custom_shimmer.dart';
import 'package:asap_weather/presentation/_core/widgets/dialog/processing_dialog.dart';
import 'package:asap_weather/presentation/screens/home/bloc/city/city_bloc.dart';
import 'package:asap_weather/presentation/screens/home/bloc/setting/setting_bloc.dart';
import 'package:asap_weather/presentation/screens/home/bloc/weather/weather_bloc.dart';
import 'package:asap_weather/presentation/screens/home/widgets/daily_forecast_item.dart';
import 'package:asap_weather/presentation/screens/home/widgets/header.dart';
import 'package:asap_weather/presentation/screens/home/widgets/hourly_temperature_chart.dart';
import 'package:asap_weather/presentation/screens/home/widgets/settings_choice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart' as l;

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  l.Location location = l.Location();
  var isLocationEnabled = true;
  ValueNotifier<String?> selectedCity = ValueNotifier(null);
  final searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchAll();
  }

  fetchAll() async {
    context.read<WeatherBloc>().add(OnRetrieveWeather());
    context.read<SettingBloc>().add(OnLoadUnit());
  }

  onRefresh() {
    fetchAll();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<WeatherBloc, WeatherState>(listener: (context, state) {
          if (state is WeatherFailure) {
            Helpers.showErrorDialog(context,
                message: state.message, onTryAgain: onRefresh);
          }
        }),
        BlocListener<SettingBloc, SettingState>(listener: (context, state) {
          if (state is SettingLoading) {
            showProcessingDialog(context: context, title: 'Adjusting Settings');
          }
          if (state is SettingSuccess) {
            Navigator.of(context).pop();
          }
          if (state is SettingLoaded) {
            Navigator.of(context).pop();
          }
        })
      ],
      child: Scaffold(
        backgroundColor: AppColor.celestialBlue,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BlocBuilder<WeatherBloc, WeatherState>(
                builder: (context, state) {
                  if (state is WeatherLoaded) {
                    return Container(
                      padding: const EdgeInsets.only(
                        right: AppConstant.kDefaultPadding,
                      ),
                      color: AppColor.celestialBlue,
                      width: double.infinity,
                      child: Row(
                        children: [
                          Material(
                            color: Colors.transparent,
                            child: InkWell(
                              onTap: () {
                                Scaffold.of(context).openDrawer();
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: AppConstant.kDefaultPadding,
                                    vertical: AppConstant.kDefaultPadding),
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                ),
                                child: const Icon(
                                  Icons.menu,
                                  color: AppColor.neutral1,
                                  size: 30,
                                ),
                              ),
                            ),
                          ),
                          RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                text: '${state.weather.locName} ',
                                style: AppTextStyle.headlineSmall.copyWith(
                                    color: AppColor.neutral1,
                                    fontWeight: FontWeight.bold),
                              ),
                              const WidgetSpan(
                                child: Icon(
                                  Icons.location_pin,
                                  color: AppColor.neutral1,
                                ),
                              )
                            ]),
                          ),
                        ],
                      ),
                    );
                  }
                  if (state is WeatherLoading) {
                    return Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppConstant.kDefaultPadding,
                          vertical: AppConstant.kDefaultPadding / 2 * 3),
                      child: CustomShimmer(
                          child: TextPlaceHolder(
                        height: 20,
                        width: MediaQuery.of(context).size.width / 2,
                      )),
                    );
                  }
                  return Container();
                },
              ),
              BlocBuilder<SettingBloc, SettingState>(
                  builder: (context, settingState) {
                if (settingState is SettingLoaded) {
                  final isMetric = settingState.unitOfMeasurements ==
                      UnitOfMeasurements.metric;
                  return Expanded(
                    child: RefreshIndicator(
                      color: AppColor.celestialBlue,
                      onRefresh: () {
                        onRefresh();
                        return Future(() => true);
                      },
                      child: SingleChildScrollView(
                        padding: const EdgeInsets.symmetric(
                            horizontal: AppConstant.kDefaultPadding,
                            vertical: AppConstant.kDefaultPadding / 2),
                        physics: const AlwaysScrollableScrollPhysics(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            BlocBuilder<WeatherBloc, WeatherState>(
                              builder: (context, state) {
                                if (state is WeatherLoaded) {
                                  final current = state.weather.currentWeather;
                                  return Header(
                                    temp: isMetric
                                        ? current.tempC.toInt()
                                        : current.tempF.toInt(),
                                    feelsLike: isMetric
                                        ? current.feelsLikeC.toInt()
                                        : current.feelsLikeF.toInt(),
                                    status: current.status.toTitleCase(),
                                    icon: current.iconCode,
                                    humidity: current.humidity.toInt(),
                                    cloud: current.cloud.toInt(),
                                    pressure: isMetric
                                        ? current.pressureBar.toDouble()
                                        : current.pressurePsi.toDouble(),
                                    windSpeed: isMetric
                                        ? current.windSpeedKph.toDouble()
                                        : current.windSpeedMph.toDouble(),
                                    isMetric: isMetric,
                                  );
                                }
                                if (state is WeatherLoading) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const CustomShimmer(
                                        child: TextPlaceHolder(
                                          height: 100,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: AppConstant.kDefaultPadding * 2,
                                      ),
                                      CustomShimmer(
                                          child: TextPlaceHolder(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2,
                                      )),
                                      const SizedBox(
                                        height: AppConstant.kDefaultPadding * 2,
                                      ),
                                      const CustomShimmer(
                                          child: TextPlaceHolder(
                                        height: 120,
                                      ))
                                    ],
                                  );
                                }
                                return Container();
                              },
                            ),
                            const SizedBox(
                              height: AppConstant.kDefaultPadding,
                            ),
                            BlocBuilder<WeatherBloc, WeatherState>(
                              builder: (context, state) {
                                if (state is WeatherLoaded) {
                                  return Container(
                                    width: double.infinity,
                                    height: 200,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: AppColor.mediumPersianBlue
                                          .withOpacity(0.2),
                                    ),
                                    child: SingleChildScrollView(
                                      physics: const BouncingScrollPhysics(
                                          decelerationRate:
                                              ScrollDecelerationRate.fast),
                                      padding: const EdgeInsets.symmetric(
                                          vertical:
                                              AppConstant.kDefaultPadding / 2),
                                      scrollDirection: Axis.horizontal,
                                      child: HourlyTemperatureChart(
                                          forecasts:
                                              state.weather.hourlyForecast,
                                          isMetric: isMetric),
                                    ),
                                  );
                                }
                                if (state is WeatherLoading) {
                                  return const CustomShimmer(
                                      child: TextPlaceHolder(
                                    height: 200,
                                  ));
                                }
                                return Container();
                              },
                            ),
                            const SizedBox(
                              height: AppConstant.kDefaultPadding,
                            ),
                            BlocBuilder<WeatherBloc, WeatherState>(
                              builder: (context, state) {
                                if (state is WeatherLoaded) {
                                  return Container(
                                      width: double.infinity,
                                      padding: const EdgeInsets.all(
                                          AppConstant.kDefaultPadding),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: AppColor.mediumPersianBlue
                                            .withOpacity(0.2),
                                      ),
                                      child: ListView.separated(
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemBuilder: (context, index) {
                                            final item = state
                                                .weather.dailyForecast
                                                .elementAt(index);
                                            return DailyForecastItem(
                                              dateTime: item.dateTime,
                                              humidity: item.humidity,
                                              icon: item.icon,
                                              minTempC: item.minTempC.toInt(),
                                              maxTempC: item.maxTempC.toInt(),
                                              maxTempF: item.maxTempF.toInt(),
                                              minTempF: item.minTempF.toInt(),
                                              isMetric: isMetric,
                                            );
                                          },
                                          separatorBuilder: (c, i) {
                                            if (i !=
                                                state.weather.dailyForecast
                                                    .length) {
                                              return const SizedBox(
                                                height: AppConstant
                                                        .kDefaultPadding /
                                                    2,
                                              );
                                            }
                                            return Container();
                                          },
                                          itemCount: state
                                              .weather.dailyForecast.length));
                                }
                                if (state is WeatherLoading) {
                                  return const CustomShimmer(
                                      child: TextPlaceHolder(
                                    height: 300,
                                  ));
                                }
                                return Container();
                              },
                            ),
                            const SizedBox(
                              height: AppConstant.kDefaultPadding * 2,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
                return Container();
              })
            ],
          ),
        ),
        drawer: SafeArea(
          child: Drawer(
            backgroundColor: AppColor.celestialBlue.withOpacity(0.9),
            child: Padding(
              padding: const EdgeInsets.all(AppConstant.kDefaultPadding),
              child: Column(
                children: [
                  BlocBuilder<CityBloc, CityState>(
                    builder: (context, state) {
                      return Column(
                        children: [
                          TextField(
                            style: AppTextStyle.bodyLarge
                                .copyWith(color: AppColor.neutral1),
                            controller: searchController,
                            cursorColor: AppColor.neutral1,
                            decoration: InputDecoration(
                              isDense: true,
                              border: OutlineInputBorder(
                                borderSide:
                                    const BorderSide(color: AppColor.neutral1),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    const BorderSide(color: AppColor.neutral1),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    const BorderSide(color: AppColor.neutral1),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              hintStyle: AppTextStyle.bodyLarge.copyWith(
                                  color: AppColor.neutral1.withOpacity(0.7)),
                              hintText: 'Search your city...',
                              suffixIcon: const Icon(
                                Icons.search,
                                color: AppColor.neutral1,
                              ),
                            ),
                            onChanged: (search) {
                              context
                                  .read<CityBloc>()
                                  .add(OnRetrieveCity(search));
                            },
                          ),
                          Stack(
                            children: [
                              Column(
                                children: [
                                  const SizedBox(
                                    height: AppConstant.kDefaultPadding,
                                  ),
                                  ValueListenableBuilder(
                                      valueListenable: selectedCity,
                                      builder: (context, value, widget) {
                                        if (value != null) {
                                          return Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                'Your location',
                                                style: AppTextStyle.bodyLarge
                                                    .copyWith(
                                                        color:
                                                            AppColor.neutral1,
                                                        fontWeight:
                                                            FontWeight.bold),
                                              ),
                                              Container(
                                                padding: const EdgeInsets.all(
                                                    AppConstant
                                                        .kDefaultPadding),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  border: Border.all(
                                                      color: AppColor.neutral1),
                                                ),
                                                width: double.infinity,
                                                child: Column(
                                                  children: [
                                                    SettingsChoice(
                                                        onTap: () {},
                                                        title: value,
                                                        selected: true),
                                                    const SizedBox(
                                                      height: AppConstant
                                                              .kDefaultPadding /
                                                          2,
                                                    ),
                                                    SettingsChoice(
                                                        onTap: () {
                                                          selectedCity.value =
                                                              null;
                                                          fetchAll();
                                                        },
                                                        title:
                                                            'Use your location',
                                                        selected: false)
                                                  ],
                                                ),
                                              ),
                                              const SizedBox(
                                                height:
                                                    AppConstant.kDefaultPadding,
                                              ),
                                            ],
                                          );
                                        }
                                        return Container();
                                      }),
                                  BlocBuilder<SettingBloc, SettingState>(
                                      builder: (context, settingState) {
                                    if (settingState is SettingLoaded) {
                                      return Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Unit of Measurement',
                                            style: AppTextStyle.bodyLarge
                                                .copyWith(
                                                    color: AppColor.neutral1,
                                                    fontWeight:
                                                        FontWeight.bold),
                                          ),
                                          Container(
                                            padding: const EdgeInsets.all(
                                                AppConstant.kDefaultPadding),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              border: Border.all(
                                                  color: AppColor.neutral1),
                                            ),
                                            width: double.infinity,
                                            child: Column(
                                              children: [
                                                SettingsChoice(
                                                    onTap: () {
                                                      context
                                                          .read<SettingBloc>()
                                                          .add(OnChangeUnit(
                                                              UnitOfMeasurements
                                                                  .metric));
                                                    },
                                                    title: 'Metric',
                                                    selected: settingState
                                                            .unitOfMeasurements ==
                                                        UnitOfMeasurements
                                                            .metric),
                                                const SizedBox(
                                                  height: AppConstant
                                                          .kDefaultPadding /
                                                      2,
                                                ),
                                                SettingsChoice(
                                                    onTap: () {
                                                      context
                                                          .read<SettingBloc>()
                                                          .add(OnChangeUnit(
                                                              UnitOfMeasurements
                                                                  .imperial));
                                                    },
                                                    title: 'Imperial',
                                                    selected: settingState
                                                            .unitOfMeasurements ==
                                                        UnitOfMeasurements
                                                            .imperial)
                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    }
                                    return Container();
                                  }),
                                ],
                              ),
                              if (state is CityLoaded) ...[
                                Container(
                                  clipBehavior: Clip.antiAlias,
                                  height: state.cities.length > 4
                                      ? 200
                                      : 55 * state.cities.length.toDouble(),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: AppColor.neutral1.withOpacity(0.9),
                                  ),
                                  child: ListView.builder(
                                      itemCount: state.cities.length,
                                      itemBuilder: (context, index) {
                                        return Material(
                                          color: Colors.transparent,
                                          child: ListTile(
                                            onTap: () {
                                              context.read<WeatherBloc>().add(
                                                  OnRetrieveWeather(
                                                      queryId: state
                                                          .cities[index].id
                                                          .toString()));
                                              context
                                                  .read<CityBloc>()
                                                  .add(OnResetCity());
                                              searchController.clear();
                                              selectedCity.value =
                                                  '${state.cities[index].name}, ${state.cities[index].region}';
                                            },
                                            title: Text(
                                              '${state.cities[index].name}, ${state.cities[index].region}',
                                              style: AppTextStyle.bodyLarge
                                                  .copyWith(
                                                      color: AppColor.neutral8,
                                                      fontWeight:
                                                          FontWeight.bold),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                              ],
                              if (state is CityLoading) ...[
                                Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: AppColor.neutral1.withOpacity(0.9),
                                  ),
                                  child: const Center(
                                    child: CircularProgressIndicator(
                                      color: AppColor.mediumPersianBlue,
                                    ),
                                  ),
                                ),
                              ]
                            ],
                          ),
                        ],
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
