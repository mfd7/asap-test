part of 'weather_bloc.dart';

@immutable
abstract class WeatherEvent {}

class OnRetrieveWeather extends WeatherEvent {
  final String? queryId;

  OnRetrieveWeather({this.queryId});
}
