import 'package:asap_weather/domain/weather/entities/weather.dart';
import 'package:asap_weather/domain/weather/usecases/retrieve_weather_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'weather_event.dart';
part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final RetrieveWeatherUsecase _retrieveWeatherUsecase;
  WeatherBloc(this._retrieveWeatherUsecase) : super(WeatherInitial()) {
    on<OnRetrieveWeather>((event, emit) async {
      emit(WeatherLoading());
      final retrieve =
          await _retrieveWeatherUsecase.execute(queryId: event.queryId);
      retrieve.fold((failure) {
        emit(WeatherFailure(failure.message));
      }, (data) {
        emit(WeatherLoaded(data));
      });
    });
  }
}
