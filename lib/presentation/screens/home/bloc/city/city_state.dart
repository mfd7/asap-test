part of 'city_bloc.dart';

@immutable
abstract class CityState extends Equatable {
  const CityState();

  @override
  List<Object?> get props => [];
}

class CityInitial extends CityState {}

class CityLoading extends CityState {}

class CityLoaded extends CityState {
  final List<City> cities;

  const CityLoaded(this.cities);

  @override
  List<Object?> get props => [cities];
}

class CityFailure extends CityState {
  final String message;

  const CityFailure(this.message);

  @override
  List<Object?> get props => [message];
}
