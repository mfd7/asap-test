part of 'city_bloc.dart';

@immutable
abstract class CityEvent {}

class OnRetrieveCity extends CityEvent {
  final String query;

  OnRetrieveCity(this.query);
}

class OnResetCity extends CityEvent {}
