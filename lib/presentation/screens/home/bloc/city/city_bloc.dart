import 'package:asap_weather/domain/city/entities/city.dart';
import 'package:asap_weather/domain/city/usecases/retrieve_city_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'city_event.dart';
part 'city_state.dart';

class CityBloc extends Bloc<CityEvent, CityState> {
  final RetrieveCityUsecase _retrieveCityUsecase;
  CityBloc(this._retrieveCityUsecase) : super(CityInitial()) {
    on<OnRetrieveCity>((event, emit) async {
      emit(CityLoading());
      final retrieve = await _retrieveCityUsecase.execute(event.query);
      retrieve.fold((failure) {
        emit(CityFailure(failure.message));
      }, (data) {
        emit(CityLoaded(data));
      });
    });
    on<OnResetCity>((event, emit) {
      emit(CityInitial());
    });
  }
}
