part of 'setting_bloc.dart';

@immutable
abstract class SettingState extends Equatable {
  const SettingState();

  @override
  List<Object?> get props => [];
}

class SettingInitial extends SettingState {}

class SettingLoading extends SettingState {}

class SettingLoaded extends SettingState {
  final UnitOfMeasurements unitOfMeasurements;

  const SettingLoaded(this.unitOfMeasurements);

  @override
  List<Object?> get props => [unitOfMeasurements];
}

class SettingSuccess extends SettingState {
  final String message;

  const SettingSuccess(this.message);

  @override
  List<Object?> get props => [message];
}

class SettingFailure extends SettingState {
  final String message;

  const SettingFailure(this.message);

  @override
  List<Object?> get props => [message];
}
