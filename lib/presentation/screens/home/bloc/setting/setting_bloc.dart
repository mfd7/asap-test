import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/domain/setting/usecases/change_unit_usecase.dart';
import 'package:asap_weather/domain/setting/usecases/load_unit_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'setting_event.dart';
part 'setting_state.dart';

class SettingBloc extends Bloc<SettingEvent, SettingState> {
  final ChangeUnitUsecase _changeUnitUsecase;
  final LoadUnitUsecase _loadUnitUsecase;
  SettingBloc(this._changeUnitUsecase, this._loadUnitUsecase)
      : super(SettingInitial()) {
    on<OnChangeUnit>((event, emit) async {
      emit(SettingLoading());
      final change = await _changeUnitUsecase.execute(event.unit);
      change.fold((failure) {
        emit(SettingFailure(failure.message));
      }, (data) {
        emit(SettingSuccess(data));
        add(OnLoadUnit());
      });
    });
    on<OnLoadUnit>((event, emit) async {
      emit(SettingLoading());
      final change = await _loadUnitUsecase.execute();
      change.fold((failure) {
        emit(SettingFailure(failure.message));
      }, (data) {
        emit(SettingLoaded(data));
      });
    });
  }
}
