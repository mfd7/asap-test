part of 'setting_bloc.dart';

@immutable
abstract class SettingEvent {}

class OnChangeUnit extends SettingEvent {
  final UnitOfMeasurements unit;

  OnChangeUnit(this.unit);
}

class OnLoadUnit extends SettingEvent {}
