import 'package:asap_weather/_core/constants/app_constant.dart';
import 'package:asap_weather/_core/utils/helpers.dart';
import 'package:asap_weather/presentation/_core/app_color.dart';
import 'package:asap_weather/presentation/_core/app_text_style.dart';
import 'package:asap_weather/presentation/screens/home/widgets/main_info_item.dart';
import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';

class Header extends StatelessWidget {
  const Header({
    super.key,
    required this.temp,
    required this.feelsLike,
    required this.status,
    required this.icon,
    required this.humidity,
    required this.cloud,
    required this.pressure,
    required this.windSpeed,
    required this.isMetric,
  });
  final int temp;
  final int feelsLike;
  final String status;
  final num icon;
  final int humidity;
  final int cloud;
  final double pressure;
  final double windSpeed;
  final bool isMetric;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$temp°',
                    style: AppTextStyle.displayLarge
                        .copyWith(color: AppColor.neutral1),
                  ),
                  const SizedBox(
                    height: AppConstant.kDefaultPadding / 2,
                  ),
                  Text(
                    status,
                    style: AppTextStyle.titleMedium.copyWith(
                        color: AppColor.neutral1, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            Expanded(
              child: Icon(
                Helpers.getWeatherIcon(icon.toInt()),
                color: AppColor.neutral1,
                size: 80,
              ),
            )
          ],
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding * 2,
        ),
        Text(
          '$temp° Feels like $feelsLike°',
          style: AppTextStyle.titleMedium
              .copyWith(color: AppColor.neutral1, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding * 2,
        ),
        Container(
          width: double.infinity,
          height: 130,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppColor.mediumPersianBlue.withOpacity(0.2),
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(
                horizontal: AppConstant.kDefaultPadding,
                vertical: AppConstant.kDefaultPadding / 2),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                MainInfoItem(
                  icon: WeatherIcons.humidity,
                  value: '$humidity%',
                  title: 'Humidity',
                ),
                const SizedBox(
                  width: AppConstant.kDefaultPadding,
                ),
                MainInfoItem(
                  icon: WeatherIcons.cloudy,
                  value: '$cloud%',
                  title: 'Cloudiness',
                ),
                const SizedBox(
                  width: AppConstant.kDefaultPadding,
                ),
                MainInfoItem(
                  icon: WeatherIcons.barometer,
                  value: '$pressure ${isMetric ? 'hpa' : 'inHg'}',
                  title: 'Pressure',
                ),
                const SizedBox(
                  width: AppConstant.kDefaultPadding,
                ),
                MainInfoItem(
                  icon: WeatherIcons.wind,
                  value: '$windSpeed ${isMetric ? 'Kph' : 'Mph'}',
                  title: 'Wind',
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
