import 'package:asap_weather/_core/constants/app_constant.dart';
import 'package:asap_weather/presentation/_core/app_color.dart';
import 'package:asap_weather/presentation/_core/app_text_style.dart';
import 'package:flutter/material.dart';

class SettingsChoice extends StatelessWidget {
  const SettingsChoice(
      {super.key,
      required this.onTap,
      required this.title,
      required this.selected});
  final Function() onTap;
  final String title;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(8),
      child: Container(
        padding: const EdgeInsets.all(AppConstant.kDefaultPadding / 2),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: selected
              ? AppColor.neutral1.withOpacity(0.2)
              : AppColor.neutral10.withOpacity(0.3),
        ),
        width: double.infinity,
        child: Text(
          title,
          style: AppTextStyle.bodyLarge.copyWith(
              color: selected
                  ? AppColor.neutral1
                  : AppColor.neutral1.withOpacity(0.5),
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
