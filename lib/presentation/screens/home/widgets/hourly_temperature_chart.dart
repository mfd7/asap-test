import 'package:asap_weather/_core/constants/app_constant.dart';
import 'package:asap_weather/domain/weather/entities/hourly_forecast.dart';
import 'package:asap_weather/presentation/_core/app_color.dart';
import 'package:asap_weather/presentation/_core/app_text_style.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HourlyTemperatureChart extends StatelessWidget {
  final List<HourlyForecast> forecasts;
  final bool isMetric;

  const HourlyTemperatureChart(
      {Key? key, required this.forecasts, required this.isMetric})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: forecasts.length * 70,
      child: BarChart(
        BarChartData(
          alignment: BarChartAlignment.spaceAround,
          barTouchData: BarTouchData(enabled: false),
          borderData: FlBorderData(show: false),
          gridData: const FlGridData(show: false),
          titlesData: FlTitlesData(
            topTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 48,
                getTitlesWidget: (value, _) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        bottom: AppConstant.kDefaultPadding / 2),
                    child: Column(
                      children: [
                        Text(
                          DateFormat('h a')
                              .format(forecasts[value.toInt()].dateTime!),
                          style: AppTextStyle.bodyMedium
                              .copyWith(color: AppColor.neutral5),
                        ),
                        Text(
                          '${isMetric ? forecasts[value.toInt()].tempC.toInt() : forecasts[value.toInt()].tempF.toInt()}°${isMetric ? 'C' : 'F'}',
                          style: AppTextStyle.bodyMedium
                              .copyWith(color: AppColor.neutral1),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            leftTitles:
                const AxisTitles(sideTitles: SideTitles(showTitles: false)),
            rightTitles:
                const AxisTitles(sideTitles: SideTitles(showTitles: false)),
            bottomTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 28,
                getTitlesWidget: (value, _) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: AppConstant.kDefaultPadding / 2),
                    child: Column(
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              const WidgetSpan(
                                child: Icon(
                                  Icons.water_drop,
                                  color: AppColor.neutral5,
                                  size: 16,
                                ),
                              ),
                              TextSpan(
                                  text: '${forecasts[value.toInt()].humidity}%',
                                  style: AppTextStyle.bodyMedium
                                      .copyWith(color: AppColor.neutral5)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            show: true,
          ),
          barGroups: forecasts.asMap().entries.map((e) {
            return BarChartGroupData(x: e.key.toInt(), barRods: [
              BarChartRodData(
                  toY: e.value.tempC.toDouble(), color: AppColor.neutral1)
            ]);
          }).toList(),
        ),
      ),
    );
  }
}
