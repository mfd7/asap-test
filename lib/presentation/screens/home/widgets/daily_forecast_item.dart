import 'package:asap_weather/_core/constants/app_constant.dart';
import 'package:asap_weather/presentation/_core/app_color.dart';
import 'package:asap_weather/presentation/_core/app_text_style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DailyForecastItem extends StatelessWidget {
  final DateTime? dateTime;
  final num humidity;
  final String icon;
  final num maxTempC;
  final num minTempC;
  final num maxTempF;
  final num minTempF;
  final bool isMetric;
  const DailyForecastItem({
    super.key,
    this.dateTime,
    required this.humidity,
    required this.icon,
    required this.maxTempC,
    required this.minTempC,
    required this.maxTempF,
    required this.minTempF,
    required this.isMetric,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            dateTime != null
                ? dateTime!.day == DateTime.now().day
                    ? 'Today'
                    : dateTime!.day < DateTime.now().day
                        ? 'Yesterday'
                        : DateFormat('EEEE').format(dateTime!)
                : '',
            style: AppTextStyle.bodyLarge.copyWith(
                color: dateTime!.day < DateTime.now().day
                    ? AppColor.neutral1.withOpacity(0.7)
                    : AppColor.neutral1,
                fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: AppConstant.kDefaultPadding / 2),
          child: RichText(
            text: TextSpan(
              children: [
                WidgetSpan(
                  child: Icon(
                    Icons.water_drop,
                    color: dateTime!.day < DateTime.now().day
                        ? AppColor.neutral5.withOpacity(0.7)
                        : AppColor.neutral5,
                    size: 20,
                  ),
                ),
                TextSpan(
                    text: '$humidity%',
                    style: AppTextStyle.bodyLarge.copyWith(
                        color: dateTime!.day < DateTime.now().day
                            ? AppColor.neutral5.withOpacity(0.7)
                            : AppColor.neutral5,
                        fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: AppConstant.kDefaultPadding / 2),
          child: CachedNetworkImage(
            imageUrl: 'https:$icon',
            height: 30,
            errorWidget: (ctx, _, __) {
              return Container();
            },
            progressIndicatorBuilder: (context, url, _) {
              return const CircularProgressIndicator(
                color: AppColor.mediumPersianBlue,
              );
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: AppConstant.kDefaultPadding / 2),
          child: SizedBox(
            width: 35,
            child: Text(
              '${isMetric ? maxTempC : maxTempF}°',
              textAlign: TextAlign.right,
              style: AppTextStyle.bodyLarge.copyWith(
                  color: dateTime!.day < DateTime.now().day
                      ? AppColor.neutral1.withOpacity(0.7)
                      : AppColor.neutral1,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: AppConstant.kDefaultPadding / 2),
          child: SizedBox(
            width: 35,
            child: Text(
              '${isMetric ? minTempC : minTempF}°',
              textAlign: TextAlign.right,
              style: AppTextStyle.bodyLarge.copyWith(
                  color: dateTime!.day < DateTime.now().day
                      ? AppColor.neutral1.withOpacity(0.7)
                      : AppColor.neutral1,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
