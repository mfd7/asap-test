import 'package:asap_weather/_core/utils/log_util.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/authentication/usecases/get_location_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'splash_event.dart';
part 'splash_state.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final GetLocationUsecase _getLocationUsecase;
  SplashBloc(this._getLocationUsecase) : super(SplashInitial()) {
    on<OnScreenLoadedSplash>((event, emit) async {
      emit(SplashLoading());
      add(const OnLocationLoadedSplash());
    });

    on<OnLocationLoadedSplash>((event, emit) async {
      emit(SplashLoadingLocation());
      final userLocationResult = await _getLocationUsecase.execute();
      userLocationResult.fold((failure) {
        logger.e(failure);
        if (failure is LocationServiceFailure) {
          emit(SplashErrorLocationService());
        } else if (failure is LocationPermissionFailure) {
          emit(SplashErrorLocationPermission());
        } else {
          emit(SplashError(failure.message));
        }
      }, (location) {
        if (location != null && location.isNotEmpty) {
          //Load checkpoint when location not empty
          // add(const OnCheckpointLoadedSplash());
          emit(SettingsLoaded());
        } else {
          //Get location when location empty
          add(const OnLocationLoadedSplash());
        }
      });
    });
  }
}
