part of 'splash_bloc.dart';

abstract class SplashEvent {
  const SplashEvent();
}

class OnScreenLoadedSplash extends SplashEvent {
  const OnScreenLoadedSplash();
}

class OnLocationLoadedSplash extends SplashEvent {
  const OnLocationLoadedSplash();
}