import 'package:asap_weather/_core/constants/routes.dart';
import 'package:asap_weather/_core/utils/log_util.dart';
import 'package:asap_weather/presentation/_core/app_color.dart';
import 'package:asap_weather/presentation/_core/app_text_style.dart';
import 'package:asap_weather/presentation/_core/widgets/dialog/error_dialog.dart';
import 'package:asap_weather/presentation/screens/splash/bloc/splash_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with WidgetsBindingObserver {
  var _shouldCheckPermission = false;
  @override
  void initState() {
    super.initState();
    context.read<SplashBloc>().add(const OnScreenLoadedSplash());
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      logger.w(_shouldCheckPermission);
      if (_shouldCheckPermission) {
        setState(() {
          _shouldCheckPermission = false;
        });
        context.read<SplashBloc>().add(const OnScreenLoadedSplash());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is SettingsLoaded) {
          Navigator.of(context).pushReplacementNamed(Routes.homeRoute);
        }
        if (state is SplashError) {
          showErrorDialog(
              context: context,
              title: 'Error',
              message: state.message,
              buttonText: 'Try Again',
              onButtonCLick: () {
                context.read<SplashBloc>().add(const OnScreenLoadedSplash());
              });
        }
      },
      child: Scaffold(
        body: BlocBuilder<SplashBloc, SplashState>(
          builder: (context, state) {
            logger.e(state);
            if (state is SplashLoadingLocation) {
              return const LoadingSplash(message: 'Getting your location');
            } else if (state is SplashLoading) {
              return const LoadingSplash(
                message: 'Loading your settings',
              );
            } else if (state is SplashErrorLocationPermission) {
              return ErrorSplash(
                message: 'We need location permission to get your location',
                buttonText: 'Open Setting',
                onTap: () {
                  setState(() {
                    _shouldCheckPermission = true;
                  });
                  Geolocator.openAppSettings();
                },
              );
            } else if (state is SplashErrorLocationService) {
              return ErrorSplash(
                message: 'Enable your GPS',
                buttonText: 'Open Setting',
                onTap: () {
                  setState(() {
                    _shouldCheckPermission = true;
                  });
                  Geolocator.openLocationSettings();
                },
              );
            }

            return Container();
          },
        ),
      ),
    );
  }
}

class LoadingSplash extends StatelessWidget {
  const LoadingSplash({Key? key, this.message}) : super(key: key);
  final String? message;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Stack(
            children: [
              CircularProgressIndicator(
                backgroundColor: AppColor.celestialBlue,
              ),
              CircularProgressIndicator(
                strokeWidth: 6.0,
                backgroundColor: Colors.transparent,
                valueColor: AlwaysStoppedAnimation<Color>(
                  AppColor.mediumPersianBlue,
                ),
              ),
            ],
          ),
          if (message != null && message!.isNotEmpty) ...[
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 20,
              ),
              child: Text(
                message!,
                style: AppTextStyle.bodyLarge.copyWith(
                  color: AppColor.neutral8,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ],
      ),
    );
  }
}

class ErrorSplash extends StatelessWidget {
  const ErrorSplash({
    Key? key,
    required this.message,
    required this.buttonText,
    this.onTap,
  }) : super(key: key);
  final String message;
  final String buttonText;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 20,
            ),
            child: Icon(
              Icons.warning_amber,
              color: Colors.amber,
              size: 40,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              message,
              style: AppTextStyle.bodyLarge.copyWith(
                color: AppColor.neutral8,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          InkWell(
            onTap: onTap,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 20,
              ),
              child: Text(
                buttonText,
                style: AppTextStyle.titleMedium.copyWith(
                  color: AppColor.celestialBlue,
                  decoration: TextDecoration.underline,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
