import 'package:asap_weather/presentation/screens/home/bloc/city/city_bloc.dart';
import 'package:asap_weather/presentation/screens/home/bloc/setting/setting_bloc.dart';
import 'package:asap_weather/presentation/screens/home/bloc/weather/weather_bloc.dart';
import 'package:asap_weather/presentation/screens/splash/bloc/splash_bloc.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerFactory(() => SplashBloc(locator()));
  locator.registerFactory(() => SettingBloc(locator(), locator()));
  locator.registerFactory(() => WeatherBloc(locator()));
  locator.registerFactory(() => CityBloc(locator()));
}
