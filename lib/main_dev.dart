import 'package:asap_weather/_core/constants/app_flavor.dart';
import 'package:asap_weather/_core/my_app.dart';
import 'package:flutter/material.dart';
import 'package:asap_weather/_core/injection.dart' as di;

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  AppFlavor(
    baseUrl: 'http://api.weatherapi.com/v1',
    flavor: Flavor.dev,
  );
  runApp(const MyApp());
}
