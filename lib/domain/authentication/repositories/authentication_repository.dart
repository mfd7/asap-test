import 'package:asap_weather/data/_core/failure.dart';
import 'package:dartz/dartz.dart';

abstract class AuthenticationRepository {
  Future<Either<Failure, String?>> getLocation();
}
