import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/authentication/repositories/authentication_repository.dart';
import 'package:dartz/dartz.dart';

class GetLocationUsecase {
  final AuthenticationRepository repository;

  GetLocationUsecase(this.repository);

  Future<Either<Failure, String?>> execute() {
    return repository.getLocation();
  }
}
