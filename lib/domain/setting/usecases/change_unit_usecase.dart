import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/setting/repositories/setting_repository.dart';
import 'package:dartz/dartz.dart';

class ChangeUnitUsecase {
  final SettingRepository settingRepository;

  ChangeUnitUsecase(this.settingRepository);
  Future<Either<Failure, String>> execute(UnitOfMeasurements unit) async {
    return settingRepository.changeUnit(unit);
  }
}
