import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/setting/repositories/setting_repository.dart';
import 'package:dartz/dartz.dart';

class LoadUnitUsecase {
  final SettingRepository settingRepository;

  LoadUnitUsecase(this.settingRepository);
  Future<Either<Failure, UnitOfMeasurements>> execute() async {
    return await settingRepository.loadUnit();
  }
}
