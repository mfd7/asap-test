import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:dartz/dartz.dart';

abstract class SettingRepository {
  Future<Either<Failure, String>> changeUnit(UnitOfMeasurements unit);
  Future<Either<Failure, UnitOfMeasurements>> loadUnit();
}
