import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/weather/entities/weather.dart';
import 'package:asap_weather/domain/weather/repositories/weather_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveWeatherUsecase {
  final WeatherRepository weatherRepository;

  RetrieveWeatherUsecase(this.weatherRepository);
  Future<Either<Failure, Weather>> execute({String? queryId}) async {
    return await weatherRepository.retrieveWeather(queryId: queryId);
  }
}
