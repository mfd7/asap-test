class DailyForecast {
  final DateTime? dateTime;
  final num humidity;
  final String icon;
  final num maxTempC;
  final num minTempC;
  final num maxTempF;
  final num minTempF;

  DailyForecast({
    this.dateTime,
    this.humidity = 0,
    this.icon = '',
    this.maxTempC = 0,
    this.minTempC = 0,
    this.maxTempF = 0,
    this.minTempF = 0,
  });
}
