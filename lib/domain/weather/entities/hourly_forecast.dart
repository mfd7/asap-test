class HourlyForecast {
  final DateTime? dateTime;
  final num tempC;
  final num tempF;
  final num humidity;

  const HourlyForecast(
      {this.dateTime, this.tempC = 0, this.tempF = 0, this.humidity = 0});
}
