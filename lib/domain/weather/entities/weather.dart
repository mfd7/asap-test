import 'package:asap_weather/domain/weather/entities/current.dart';
import 'package:asap_weather/domain/weather/entities/daily_forecast.dart';
import 'package:asap_weather/domain/weather/entities/hourly_forecast.dart';

class Weather {
  final String locName;
  final Current currentWeather;
  final List<HourlyForecast> hourlyForecast;
  final List<DailyForecast> dailyForecast;

  Weather(
      {this.locName = '',
      this.currentWeather = const Current(),
      this.hourlyForecast = const [],
      this.dailyForecast = const []});
}
