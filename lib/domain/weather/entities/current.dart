class Current {
  final String name;
  final num tempC;
  final num tempF;
  final String status;
  final num feelsLikeC;
  final num feelsLikeF;
  final num humidity;
  final num pressureBar;
  final num pressurePsi;
  final num windSpeedKph;
  final num windSpeedMph;
  final num cloud;
  final num iconCode;

  const Current({
    this.name = '',
    this.tempC = 0,
    this.status = '',
    this.feelsLikeC = 0,
    this.humidity = 0,
    this.pressureBar = 0,
    this.windSpeedKph = 0,
    this.cloud = 0,
    this.iconCode = 0,
    this.tempF = 0,
    this.feelsLikeF = 0,
    this.pressurePsi = 0,
    this.windSpeedMph = 0,
  });
}
