import 'package:asap_weather/data/_core/failure.dart';

import 'package:asap_weather/domain/weather/entities/weather.dart';
import 'package:dartz/dartz.dart';

abstract class WeatherRepository {
  Future<Either<Failure, Weather>> retrieveWeather({String? queryId});
}
