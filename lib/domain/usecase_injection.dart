import 'package:asap_weather/domain/authentication/usecases/get_location_usecase.dart';
import 'package:asap_weather/domain/city/usecases/retrieve_city_usecase.dart';
import 'package:asap_weather/domain/setting/usecases/change_unit_usecase.dart';
import 'package:asap_weather/domain/setting/usecases/load_unit_usecase.dart';
import 'package:asap_weather/domain/weather/usecases/retrieve_weather_usecase.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton(() => GetLocationUsecase(locator()));
  locator.registerLazySingleton(() => RetrieveWeatherUsecase(locator()));
  locator.registerLazySingleton(() => RetrieveCityUsecase(locator()));
  locator.registerLazySingleton(() => ChangeUnitUsecase(locator()));
  locator.registerLazySingleton(() => LoadUnitUsecase(locator()));
}
