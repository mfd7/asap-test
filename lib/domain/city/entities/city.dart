class City {
  final int id;
  final String name;
  final String region;

  City({this.id = 0, this.name = '', this.region = ''});
}
