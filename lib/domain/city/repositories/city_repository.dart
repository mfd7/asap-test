import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/city/entities/city.dart';
import 'package:dartz/dartz.dart';

abstract class CityRepository {
  Future<Either<Failure, List<City>>> retrieveCities(String query);
}
