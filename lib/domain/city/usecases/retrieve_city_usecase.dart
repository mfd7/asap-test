import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/domain/city/entities/city.dart';
import 'package:asap_weather/domain/city/repositories/city_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveCityUsecase {
  final CityRepository cityRepository;

  RetrieveCityUsecase(this.cityRepository);

  Future<Either<Failure, List<City>>> execute(String query) async {
    return await cityRepository.retrieveCities(query);
  }
}
