enum Flavor { dev, prod, sit }

class AppFlavor {
  final Flavor flavor;
  final String baseUrl;

  static late AppFlavor _instance;
  static bool initialize = false;

  factory AppFlavor({
    required flavor,
    required baseUrl,
  }) {
    initialize = true;
    _instance = AppFlavor._initialize(flavor, baseUrl);
    return _instance;
  }

  AppFlavor._initialize(this.flavor, this.baseUrl);

  static AppFlavor get instance {
    return _instance;
  }

  static bool get isDev => instance.flavor == Flavor.dev;
  static bool get isProd => instance.flavor == Flavor.prod;
}
