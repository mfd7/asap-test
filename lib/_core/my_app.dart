import 'package:asap_weather/_core/app_router.dart';
import 'package:asap_weather/_core/constants/routes.dart';
import 'package:asap_weather/presentation/screens/home/bloc/city/city_bloc.dart';
import 'package:asap_weather/presentation/screens/home/bloc/setting/setting_bloc.dart';
import 'package:asap_weather/presentation/screens/home/bloc/weather/weather_bloc.dart';
import 'package:asap_weather/presentation/screens/splash/bloc/splash_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_alice/alice.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:asap_weather/_core/injection.dart' as di;

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => di.locator<SplashBloc>()),
        BlocProvider(create: (_) => di.locator<SettingBloc>()),
        BlocProvider(create: (_) => di.locator<WeatherBloc>()),
        BlocProvider(create: (_) => di.locator<CityBloc>()),
      ],
      child: OverlaySupport.global(
        child: MaterialApp(
          navigatorKey: di.locator<Alice>().getNavigatorKey(),
          title: 'ASAP Weather',
          onGenerateRoute: AppRouter().generateRoute,
          initialRoute: Routes.splashRoute,
        ),
      ),
    );
  }
}
