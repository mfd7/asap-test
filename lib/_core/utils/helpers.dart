import 'package:asap_weather/presentation/_core/app_icon.dart';
import 'package:flutter/material.dart';

class Helpers {
  static Future<void> showErrorDialog(BuildContext context,
      {required String message, required Function onTryAgain}) async {
    if (ModalRoute.of(context)?.isCurrent != true) {
      return;
    }
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Error'),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              style: TextButton.styleFrom(
                textStyle: Theme.of(context).textTheme.labelLarge,
              ),
              child: const Text('Try Again'),
              onPressed: () {
                Navigator.of(context).pop();
                onTryAgain();
              },
            ),
          ],
        );
      },
    );
  }

  static IconData getWeatherIcon(int code) {
    DateTime now = DateTime.now();

    DateTime sunrise = DateTime(now.year, now.month, now.day, 6, 0);
    DateTime sunset = DateTime(now.year, now.month, now.day, 18, 0);

    final isDayTime = now.isAfter(sunrise) && now.isBefore(sunset);

    final icon =
        AppIcon.weatherIcon.firstWhere((element) => element['code'] == code)[
            isDayTime ? 'dayIcon' : 'nightIcon'] as IconData;

    return icon;
  }
}
