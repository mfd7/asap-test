import 'package:asap_weather/data/authentication/datasources/authentication_local_datasource.dart';
import 'package:asap_weather/data/city/datasources/city_remote_datasource.dart';
import 'package:asap_weather/data/setting/datasources/setting_local_datasource.dart';
import 'package:asap_weather/data/weather/datasources/weather_remote_datasource.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton<AuthenticationLocalDatasource>(
      () => AuthenticationLocalDatasourceImpl(locator()));
  locator.registerLazySingleton<WeatherRemoteDatasource>(
      () => WeatherRemoteDatasourceImpl(locator()));
  locator.registerLazySingleton<CityRemoteDatasource>(
      () => CityRemoteDatasourceImpl(locator()));
  locator.registerLazySingleton<SettingLocalDatasource>(
      () => SettingLocalDatasourceImpl(locator()));
}
