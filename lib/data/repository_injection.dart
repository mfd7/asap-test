import 'package:asap_weather/data/authentication/repositories/authentication_repository_impl.dart';
import 'package:asap_weather/data/city/repositories/city_repository_impl.dart';
import 'package:asap_weather/data/setting/repositories/setting_repository_impl.dart';
import 'package:asap_weather/data/weather/repositories/weather_repository_impl.dart';
import 'package:asap_weather/domain/authentication/repositories/authentication_repository.dart';
import 'package:asap_weather/domain/city/repositories/city_repository.dart';
import 'package:asap_weather/domain/setting/repositories/setting_repository.dart';
import 'package:asap_weather/domain/weather/repositories/weather_repository.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton<AuthenticationRepository>(
      () => AuthenticationRepositoryImpl(locator()));
  locator.registerLazySingleton<WeatherRepository>(
      () => WeatherRepositoryImpl(locator()));
  locator.registerLazySingleton<CityRepository>(
      () => CityRepositoryImpl(locator()));
  locator.registerLazySingleton<SettingRepository>(
      () => SettingRepositoryImpl(locator()));
}
