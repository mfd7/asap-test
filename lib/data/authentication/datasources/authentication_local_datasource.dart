import 'package:asap_weather/_core/utils/log_util.dart';
import 'package:asap_weather/data/_core/exception.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';

abstract class AuthenticationLocalDatasource {
  Future<String?> getLocation();
}

class AuthenticationLocalDatasourceImpl
    implements AuthenticationLocalDatasource {
  final Location gpsLocation;

  AuthenticationLocalDatasourceImpl(this.gpsLocation);

  @override
  Future<String?> getLocation() async {
    await _checkLocationServiceAndPermission();
    final locationData = await Geolocator.getCurrentPosition();
    final latLang = '${locationData.latitude},${locationData.longitude}';
    return latLang;
  }

  Future _checkLocationServiceAndPermission() async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        throw LocationPermissionException();
      }
    }
    var serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await gpsLocation.requestService();
      logger.e("serviceEnabled: $serviceEnabled");
      if (!serviceEnabled) {
        throw LocationServiceException();
      }
    }
  }
}
