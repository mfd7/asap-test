import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/data/authentication/datasources/authentication_local_datasource.dart';
import 'package:asap_weather/domain/authentication/repositories/authentication_repository.dart';
import 'package:dartz/dartz.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationLocalDatasource localDatasource;

  AuthenticationRepositoryImpl(this.localDatasource);
  @override
  Future<Either<Failure, String?>> getLocation() async {
    try {
      final result = await localDatasource.getLocation();
      return Right(result);
    } on LocationServiceException {
      return const Left(LocationServiceFailure());
    } on LocationPermissionException {
      return const Left(LocationPermissionFailure());
    }
  }
}
