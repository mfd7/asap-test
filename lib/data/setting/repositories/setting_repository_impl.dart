import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/data/setting/datasources/setting_local_datasource.dart';
import 'package:asap_weather/domain/setting/repositories/setting_repository.dart';
import 'package:dartz/dartz.dart';

class SettingRepositoryImpl implements SettingRepository {
  final SettingLocalDatasource settingLocalDatasource;

  SettingRepositoryImpl(this.settingLocalDatasource);
  @override
  Future<Either<Failure, String>> changeUnit(UnitOfMeasurements unit) async {
    try {
      await settingLocalDatasource.changeUnit(unit);
      return const Right('Unit Changed');
    } on LocationServiceException {
      return const Left(LocationServiceFailure());
    } on LocationPermissionException {
      return const Left(LocationPermissionFailure());
    }
  }

  @override
  Future<Either<Failure, UnitOfMeasurements>> loadUnit() async {
    try {
      final unit = await settingLocalDatasource.loadUnit();
      return Right(unit);
    } on LocationServiceException {
      return const Left(LocationServiceFailure());
    } on LocationPermissionException {
      return const Left(LocationPermissionFailure());
    }
  }
}
