class SettingsModel {
  SettingsModel({
    String? unitOfMeasurement,
  }) {
    _unitOfMeasurement = unitOfMeasurement;
  }

  SettingsModel.fromJson(dynamic json) {
    _unitOfMeasurement = json['unit_of_measurement'];
  }
  String? _unitOfMeasurement;
  SettingsModel copyWith({
    String? unitOfMeasurement,
  }) =>
      SettingsModel(
        unitOfMeasurement: unitOfMeasurement ?? _unitOfMeasurement,
      );
  String? get unitOfMeasurement => _unitOfMeasurement;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['unit_of_measurement'] = _unitOfMeasurement;
    return map;
  }
}
