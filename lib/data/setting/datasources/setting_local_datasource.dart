import 'package:asap_weather/_core/constants/app_enum.dart';
import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/_core/shared_preferences_helper.dart';

abstract class SettingLocalDatasource {
  Future<void> changeUnit(UnitOfMeasurements unit);
  Future<UnitOfMeasurements> loadUnit();
}

class SettingLocalDatasourceImpl implements SettingLocalDatasource {
  final SharedPreferencesHelper sharedPreferencesHelper;

  SettingLocalDatasourceImpl(this.sharedPreferencesHelper);
  @override
  Future<void> changeUnit(UnitOfMeasurements unit) async {
    var unitString = '';
    if (unit == UnitOfMeasurements.metric) {
      unitString = 'metric';
    } else {
      unitString = 'imperial';
    }
    await sharedPreferencesHelper.addString(
      SharedPreferencesKeys.unitOfMeasurement,
      unitString,
    );
  }

  @override
  Future<UnitOfMeasurements> loadUnit() async {
    try {
      final unit = await sharedPreferencesHelper
          .loadString(SharedPreferencesKeys.unitOfMeasurement);
      UnitOfMeasurements unitMapped;
      if (unit?.toLowerCase() == 'metric') {
        unitMapped = UnitOfMeasurements.metric;
      } else {
        unitMapped = UnitOfMeasurements.imperial;
      }
      return unitMapped;
    } catch (_) {
      throw ParseDataException();
    }
  }
}
