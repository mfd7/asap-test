import 'package:asap_weather/domain/city/entities/city.dart';

class CityModel {
  CityModel({
    num? id,
    String? name,
    String? region,
    String? country,
    num? lat,
    num? lon,
    String? url,
  }) {
    _id = id;
    _name = name;
    _region = region;
    _country = country;
    _lat = lat;
    _lon = lon;
    _url = url;
  }

  CityModel.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _region = json['region'];
    _country = json['country'];
    _lat = json['lat'];
    _lon = json['lon'];
    _url = json['url'];
  }
  num? _id;
  String? _name;
  String? _region;
  String? _country;
  num? _lat;
  num? _lon;
  String? _url;
  CityModel copyWith({
    num? id,
    String? name,
    String? region,
    String? country,
    num? lat,
    num? lon,
    String? url,
  }) =>
      CityModel(
        id: id ?? _id,
        name: name ?? _name,
        region: region ?? _region,
        country: country ?? _country,
        lat: lat ?? _lat,
        lon: lon ?? _lon,
        url: url ?? _url,
      );
  num get id => _id ?? 0;
  String get name => _name ?? '';
  String get region => _region ?? '';
  String get country => _country ?? '';
  num get lat => _lat ?? 0;
  num get lon => _lon ?? 0;
  String get url => _url ?? '';

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['region'] = _region;
    map['country'] = _country;
    map['lat'] = _lat;
    map['lon'] = _lon;
    map['url'] = _url;
    return map;
  }

  City toEntity() {
    return City(id: id.toInt(), name: name, region: region);
  }
}
