import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/data/city/datasources/city_remote_datasource.dart';
import 'package:asap_weather/domain/city/entities/city.dart';
import 'package:asap_weather/domain/city/repositories/city_repository.dart';
import 'package:dartz/dartz.dart';

class CityRepositoryImpl implements CityRepository {
  final CityRemoteDatasource cityRemoteDatasource;

  CityRepositoryImpl(this.cityRemoteDatasource);
  @override
  Future<Either<Failure, List<City>>> retrieveCities(String query) async {
    try {
      final responses = await cityRemoteDatasource.retrieveCities(query);
      return Right(responses.map((e) => e.toEntity()).toList());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }
}
