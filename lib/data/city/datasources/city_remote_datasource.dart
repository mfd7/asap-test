import 'package:asap_weather/data/_core/api_base_helper.dart';
import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/city/city_endpoints.dart';
import 'package:asap_weather/data/city/models/city_model.dart';

abstract class CityRemoteDatasource {
  Future<List<CityModel>> retrieveCities(String query);
}

class CityRemoteDatasourceImpl implements CityRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  CityRemoteDatasourceImpl(this.apiBaseHelper);
  @override
  Future<List<CityModel>> retrieveCities(String query) async {
    final response = await apiBaseHelper
        .getApi(CityEndpoints.searchCity, qParams: {'q': query});
    try {
      List<dynamic> parsedListJson = response;
      return List<CityModel>.from(
        parsedListJson.map<CityModel>(
          (i) => CityModel.fromJson(i),
        ),
      );
    } catch (_) {
      throw ParseDataException();
    }
  }
}
