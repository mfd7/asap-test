import 'package:asap_weather/data/weather/models/condition_model.dart';
import 'package:asap_weather/domain/weather/entities/hourly_forecast.dart';

class HourModel {
  HourModel({
    num? timeEpoch,
    String? time,
    num? tempC,
    num? tempF,
    num? isDay,
    ConditionModel? condition,
    num? windMph,
    num? windKph,
    num? windDegree,
    String? windDir,
    num? pressureMb,
    num? pressureIn,
    num? precipMm,
    num? precipIn,
    num? snowCm,
    num? humidity,
    num? cloud,
    num? feelslikeC,
    num? feelslikeF,
    num? windchillC,
    num? windchillF,
    num? heatindexC,
    num? heatindexF,
    num? dewpointC,
    num? dewpointF,
    num? willItRain,
    num? chanceOfRain,
    num? willItSnow,
    num? chanceOfSnow,
    num? visKm,
    num? visMiles,
    num? gustMph,
    num? gustKph,
    num? uv,
    num? shortRad,
    num? diffRad,
  }) {
    _timeEpoch = timeEpoch;
    _time = time;
    _tempC = tempC;
    _tempF = tempF;
    _isDay = isDay;
    _condition = condition;
    _windMph = windMph;
    _windKph = windKph;
    _windDegree = windDegree;
    _windDir = windDir;
    _pressureMb = pressureMb;
    _pressureIn = pressureIn;
    _precipMm = precipMm;
    _precipIn = precipIn;
    _snowCm = snowCm;
    _humidity = humidity;
    _cloud = cloud;
    _feelslikeC = feelslikeC;
    _feelslikeF = feelslikeF;
    _windchillC = windchillC;
    _windchillF = windchillF;
    _heatindexC = heatindexC;
    _heatindexF = heatindexF;
    _dewpointC = dewpointC;
    _dewpointF = dewpointF;
    _willItRain = willItRain;
    _chanceOfRain = chanceOfRain;
    _willItSnow = willItSnow;
    _chanceOfSnow = chanceOfSnow;
    _visKm = visKm;
    _visMiles = visMiles;
    _gustMph = gustMph;
    _gustKph = gustKph;
    _uv = uv;
    _shortRad = shortRad;
    _diffRad = diffRad;
  }

  HourModel.fromJson(dynamic json) {
    _timeEpoch = json['time_epoch'];
    _time = json['time'];
    _tempC = json['temp_c'];
    _tempF = json['temp_f'];
    _isDay = json['is_day'];
    _condition = json['condition'] != null
        ? ConditionModel.fromJson(json['condition'])
        : null;
    _windMph = json['wind_mph'];
    _windKph = json['wind_kph'];
    _windDegree = json['wind_degree'];
    _windDir = json['wind_dir'];
    _pressureMb = json['pressure_mb'];
    _pressureIn = json['pressure_in'];
    _precipMm = json['precip_mm'];
    _precipIn = json['precip_in'];
    _snowCm = json['snow_cm'];
    _humidity = json['humidity'];
    _cloud = json['cloud'];
    _feelslikeC = json['feelslike_c'];
    _feelslikeF = json['feelslike_f'];
    _windchillC = json['windchill_c'];
    _windchillF = json['windchill_f'];
    _heatindexC = json['heatindex_c'];
    _heatindexF = json['heatindex_f'];
    _dewpointC = json['dewpoint_c'];
    _dewpointF = json['dewpoint_f'];
    _willItRain = json['will_it_rain'];
    _chanceOfRain = json['chance_of_rain'];
    _willItSnow = json['will_it_snow'];
    _chanceOfSnow = json['chance_of_snow'];
    _visKm = json['vis_km'];
    _visMiles = json['vis_miles'];
    _gustMph = json['gust_mph'];
    _gustKph = json['gust_kph'];
    _uv = json['uv'];
    _shortRad = json['short_rad'];
    _diffRad = json['diff_rad'];
  }
  num? _timeEpoch;
  String? _time;
  num? _tempC;
  num? _tempF;
  num? _isDay;
  ConditionModel? _condition;
  num? _windMph;
  num? _windKph;
  num? _windDegree;
  String? _windDir;
  num? _pressureMb;
  num? _pressureIn;
  num? _precipMm;
  num? _precipIn;
  num? _snowCm;
  num? _humidity;
  num? _cloud;
  num? _feelslikeC;
  num? _feelslikeF;
  num? _windchillC;
  num? _windchillF;
  num? _heatindexC;
  num? _heatindexF;
  num? _dewpointC;
  num? _dewpointF;
  num? _willItRain;
  num? _chanceOfRain;
  num? _willItSnow;
  num? _chanceOfSnow;
  num? _visKm;
  num? _visMiles;
  num? _gustMph;
  num? _gustKph;
  num? _uv;
  num? _shortRad;
  num? _diffRad;
  HourModel copyWith({
    num? timeEpoch,
    String? time,
    num? tempC,
    num? tempF,
    num? isDay,
    ConditionModel? condition,
    num? windMph,
    num? windKph,
    num? windDegree,
    String? windDir,
    num? pressureMb,
    num? pressureIn,
    num? precipMm,
    num? precipIn,
    num? snowCm,
    num? humidity,
    num? cloud,
    num? feelslikeC,
    num? feelslikeF,
    num? windchillC,
    num? windchillF,
    num? heatindexC,
    num? heatindexF,
    num? dewpointC,
    num? dewpointF,
    num? willItRain,
    num? chanceOfRain,
    num? willItSnow,
    num? chanceOfSnow,
    num? visKm,
    num? visMiles,
    num? gustMph,
    num? gustKph,
    num? uv,
    num? shortRad,
    num? diffRad,
  }) =>
      HourModel(
        timeEpoch: timeEpoch ?? _timeEpoch,
        time: time ?? _time,
        tempC: tempC ?? _tempC,
        tempF: tempF ?? _tempF,
        isDay: isDay ?? _isDay,
        condition: condition ?? _condition,
        windMph: windMph ?? _windMph,
        windKph: windKph ?? _windKph,
        windDegree: windDegree ?? _windDegree,
        windDir: windDir ?? _windDir,
        pressureMb: pressureMb ?? _pressureMb,
        pressureIn: pressureIn ?? _pressureIn,
        precipMm: precipMm ?? _precipMm,
        precipIn: precipIn ?? _precipIn,
        snowCm: snowCm ?? _snowCm,
        humidity: humidity ?? _humidity,
        cloud: cloud ?? _cloud,
        feelslikeC: feelslikeC ?? _feelslikeC,
        feelslikeF: feelslikeF ?? _feelslikeF,
        windchillC: windchillC ?? _windchillC,
        windchillF: windchillF ?? _windchillF,
        heatindexC: heatindexC ?? _heatindexC,
        heatindexF: heatindexF ?? _heatindexF,
        dewpointC: dewpointC ?? _dewpointC,
        dewpointF: dewpointF ?? _dewpointF,
        willItRain: willItRain ?? _willItRain,
        chanceOfRain: chanceOfRain ?? _chanceOfRain,
        willItSnow: willItSnow ?? _willItSnow,
        chanceOfSnow: chanceOfSnow ?? _chanceOfSnow,
        visKm: visKm ?? _visKm,
        visMiles: visMiles ?? _visMiles,
        gustMph: gustMph ?? _gustMph,
        gustKph: gustKph ?? _gustKph,
        uv: uv ?? _uv,
        shortRad: shortRad ?? _shortRad,
        diffRad: diffRad ?? _diffRad,
      );
  num get timeEpoch => _timeEpoch ?? 0;
  String get time => _time ?? '';
  num get tempC => _tempC ?? 0;
  num get tempF => _tempF ?? 0;
  num get isDay => _isDay ?? 0;
  ConditionModel? get condition => _condition;
  num get windMph => _windMph ?? 0;
  num get windKph => _windKph ?? 0;
  num get windDegree => _windDegree ?? 0;
  String get windDir => _windDir ?? '';
  num get pressureMb => _pressureMb ?? 0;
  num get pressureIn => _pressureIn ?? 0;
  num get precipMm => _precipMm ?? 0;
  num get precipIn => _precipIn ?? 0;
  num get snowCm => _snowCm ?? 0;
  num get humidity => _humidity ?? 0;
  num get cloud => _cloud ?? 0;
  num get feelslikeC => _feelslikeC ?? 0;
  num get feelslikeF => _feelslikeF ?? 0;
  num get windchillC => _windchillC ?? 0;
  num get windchillF => _windchillF ?? 0;
  num get heatindexC => _heatindexC ?? 0;
  num get heatindexF => _heatindexF ?? 0;
  num get dewpointC => _dewpointC ?? 0;
  num get dewpointF => _dewpointF ?? 0;
  num get willItRain => _willItRain ?? 0;
  num get chanceOfRain => _chanceOfRain ?? 0;
  num get willItSnow => _willItSnow ?? 0;
  num get chanceOfSnow => _chanceOfSnow ?? 0;
  num get visKm => _visKm ?? 0;
  num get visMiles => _visMiles ?? 0;
  num get gustMph => _gustMph ?? 0;
  num get gustKph => _gustKph ?? 0;
  num get uv => _uv ?? 0;
  num get shortRad => _shortRad ?? 0;
  num get diffRad => _diffRad ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['time_epoch'] = _timeEpoch;
    map['time'] = _time;
    map['temp_c'] = _tempC;
    map['temp_f'] = _tempF;
    map['is_day'] = _isDay;
    if (_condition != null) {
      map['condition'] = _condition?.toJson();
    }
    map['wind_mph'] = _windMph;
    map['wind_kph'] = _windKph;
    map['wind_degree'] = _windDegree;
    map['wind_dir'] = _windDir;
    map['pressure_mb'] = _pressureMb;
    map['pressure_in'] = _pressureIn;
    map['precip_mm'] = _precipMm;
    map['precip_in'] = _precipIn;
    map['snow_cm'] = _snowCm;
    map['humidity'] = _humidity;
    map['cloud'] = _cloud;
    map['feelslike_c'] = _feelslikeC;
    map['feelslike_f'] = _feelslikeF;
    map['windchill_c'] = _windchillC;
    map['windchill_f'] = _windchillF;
    map['heatindex_c'] = _heatindexC;
    map['heatindex_f'] = _heatindexF;
    map['dewpoint_c'] = _dewpointC;
    map['dewpoint_f'] = _dewpointF;
    map['will_it_rain'] = _willItRain;
    map['chance_of_rain'] = _chanceOfRain;
    map['will_it_snow'] = _willItSnow;
    map['chance_of_snow'] = _chanceOfSnow;
    map['vis_km'] = _visKm;
    map['vis_miles'] = _visMiles;
    map['gust_mph'] = _gustMph;
    map['gust_kph'] = _gustKph;
    map['uv'] = _uv;
    map['short_rad'] = _shortRad;
    map['diff_rad'] = _diffRad;
    return map;
  }

  HourlyForecast toEntity() {
    return HourlyForecast(
        dateTime: DateTime.parse(time),
        tempC: tempC,
        tempF: tempF,
        humidity: humidity);
  }
}
