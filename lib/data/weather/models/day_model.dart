import 'package:asap_weather/data/weather/models/condition_model.dart';

class DayModel {
  DayModel({
    num? maxtempC,
    num? maxtempF,
    num? mintempC,
    num? mintempF,
    num? avgtempC,
    num? avgtempF,
    num? maxwindMph,
    num? maxwindKph,
    num? totalprecipMm,
    num? totalprecipIn,
    num? totalsnowCm,
    num? avgvisKm,
    num? avgvisMiles,
    num? avghumidity,
    num? dailyWillItRain,
    num? dailyChanceOfRain,
    num? dailyWillItSnow,
    num? dailyChanceOfSnow,
    ConditionModel? condition,
    num? uv,
  }) {
    _maxtempC = maxtempC;
    _maxtempF = maxtempF;
    _mintempC = mintempC;
    _mintempF = mintempF;
    _avgtempC = avgtempC;
    _avgtempF = avgtempF;
    _maxwindMph = maxwindMph;
    _maxwindKph = maxwindKph;
    _totalprecipMm = totalprecipMm;
    _totalprecipIn = totalprecipIn;
    _totalsnowCm = totalsnowCm;
    _avgvisKm = avgvisKm;
    _avgvisMiles = avgvisMiles;
    _avghumidity = avghumidity;
    _dailyWillItRain = dailyWillItRain;
    _dailyChanceOfRain = dailyChanceOfRain;
    _dailyWillItSnow = dailyWillItSnow;
    _dailyChanceOfSnow = dailyChanceOfSnow;
    _condition = condition;
    _uv = uv;
  }

  DayModel.fromJson(dynamic json) {
    _maxtempC = json['maxtemp_c'];
    _maxtempF = json['maxtemp_f'];
    _mintempC = json['mintemp_c'];
    _mintempF = json['mintemp_f'];
    _avgtempC = json['avgtemp_c'];
    _avgtempF = json['avgtemp_f'];
    _maxwindMph = json['maxwind_mph'];
    _maxwindKph = json['maxwind_kph'];
    _totalprecipMm = json['totalprecip_mm'];
    _totalprecipIn = json['totalprecip_in'];
    _totalsnowCm = json['totalsnow_cm'];
    _avgvisKm = json['avgvis_km'];
    _avgvisMiles = json['avgvis_miles'];
    _avghumidity = json['avghumidity'];
    _dailyWillItRain = json['daily_will_it_rain'];
    _dailyChanceOfRain = json['daily_chance_of_rain'];
    _dailyWillItSnow = json['daily_will_it_snow'];
    _dailyChanceOfSnow = json['daily_chance_of_snow'];
    _condition = json['condition'] != null
        ? ConditionModel.fromJson(json['condition'])
        : null;
    _uv = json['uv'];
  }
  num? _maxtempC;
  num? _maxtempF;
  num? _mintempC;
  num? _mintempF;
  num? _avgtempC;
  num? _avgtempF;
  num? _maxwindMph;
  num? _maxwindKph;
  num? _totalprecipMm;
  num? _totalprecipIn;
  num? _totalsnowCm;
  num? _avgvisKm;
  num? _avgvisMiles;
  num? _avghumidity;
  num? _dailyWillItRain;
  num? _dailyChanceOfRain;
  num? _dailyWillItSnow;
  num? _dailyChanceOfSnow;
  ConditionModel? _condition;
  num? _uv;
  DayModel copyWith({
    num? maxtempC,
    num? maxtempF,
    num? mintempC,
    num? mintempF,
    num? avgtempC,
    num? avgtempF,
    num? maxwindMph,
    num? maxwindKph,
    num? totalprecipMm,
    num? totalprecipIn,
    num? totalsnowCm,
    num? avgvisKm,
    num? avgvisMiles,
    num? avghumidity,
    num? dailyWillItRain,
    num? dailyChanceOfRain,
    num? dailyWillItSnow,
    num? dailyChanceOfSnow,
    ConditionModel? condition,
    num? uv,
  }) =>
      DayModel(
        maxtempC: maxtempC ?? _maxtempC,
        maxtempF: maxtempF ?? _maxtempF,
        mintempC: mintempC ?? _mintempC,
        mintempF: mintempF ?? _mintempF,
        avgtempC: avgtempC ?? _avgtempC,
        avgtempF: avgtempF ?? _avgtempF,
        maxwindMph: maxwindMph ?? _maxwindMph,
        maxwindKph: maxwindKph ?? _maxwindKph,
        totalprecipMm: totalprecipMm ?? _totalprecipMm,
        totalprecipIn: totalprecipIn ?? _totalprecipIn,
        totalsnowCm: totalsnowCm ?? _totalsnowCm,
        avgvisKm: avgvisKm ?? _avgvisKm,
        avgvisMiles: avgvisMiles ?? _avgvisMiles,
        avghumidity: avghumidity ?? _avghumidity,
        dailyWillItRain: dailyWillItRain ?? _dailyWillItRain,
        dailyChanceOfRain: dailyChanceOfRain ?? _dailyChanceOfRain,
        dailyWillItSnow: dailyWillItSnow ?? _dailyWillItSnow,
        dailyChanceOfSnow: dailyChanceOfSnow ?? _dailyChanceOfSnow,
        condition: condition ?? _condition,
        uv: uv ?? _uv,
      );
  num get maxtempC => _maxtempC ?? 0;
  num get maxtempF => _maxtempF ?? 0;
  num get mintempC => _mintempC ?? 0;
  num get mintempF => _mintempF ?? 0;
  num get avgtempC => _avgtempC ?? 0;
  num get avgtempF => _avgtempF ?? 0;
  num get maxwindMph => _maxwindMph ?? 0;
  num get maxwindKph => _maxwindKph ?? 0;
  num get totalprecipMm => _totalprecipMm ?? 0;
  num get totalprecipIn => _totalprecipIn ?? 0;
  num get totalsnowCm => _totalsnowCm ?? 0;
  num get avgvisKm => _avgvisKm ?? 0;
  num get avgvisMiles => _avgvisMiles ?? 0;
  num get avghumidity => _avghumidity ?? 0;
  num get dailyWillItRain => _dailyWillItRain ?? 0;
  num get dailyChanceOfRain => _dailyChanceOfRain ?? 0;
  num get dailyWillItSnow => _dailyWillItSnow ?? 0;
  num get dailyChanceOfSnow => _dailyChanceOfSnow ?? 0;
  ConditionModel get condition => _condition ?? ConditionModel();
  num get uv => _uv ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['maxtemp_c'] = _maxtempC;
    map['maxtemp_f'] = _maxtempF;
    map['mintemp_c'] = _mintempC;
    map['mintemp_f'] = _mintempF;
    map['avgtemp_c'] = _avgtempC;
    map['avgtemp_f'] = _avgtempF;
    map['maxwind_mph'] = _maxwindMph;
    map['maxwind_kph'] = _maxwindKph;
    map['totalprecip_mm'] = _totalprecipMm;
    map['totalprecip_in'] = _totalprecipIn;
    map['totalsnow_cm'] = _totalsnowCm;
    map['avgvis_km'] = _avgvisKm;
    map['avgvis_miles'] = _avgvisMiles;
    map['avghumidity'] = _avghumidity;
    map['daily_will_it_rain'] = _dailyWillItRain;
    map['daily_chance_of_rain'] = _dailyChanceOfRain;
    map['daily_will_it_snow'] = _dailyWillItSnow;
    map['daily_chance_of_snow'] = _dailyChanceOfSnow;
    if (_condition != null) {
      map['condition'] = _condition?.toJson();
    }
    map['uv'] = _uv;
    return map;
  }
}
