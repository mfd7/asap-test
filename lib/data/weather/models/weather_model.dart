import 'package:asap_weather/data/weather/models/current_model.dart';
import 'package:asap_weather/data/weather/models/forecast_model.dart';
import 'package:asap_weather/data/weather/models/location_model.dart';
import 'package:asap_weather/domain/weather/entities/weather.dart';

class WeatherModel {
  WeatherModel({
    LocationModel? location,
    CurrentModel? current,
    ForecastModel? forecast,
  }) {
    _location = location;
    _current = current;
    _forecast = forecast;
  }

  WeatherModel.fromJson(dynamic json) {
    _location = json['location'] != null
        ? LocationModel.fromJson(json['location'])
        : null;
    _current =
        json['current'] != null ? CurrentModel.fromJson(json['current']) : null;
    _forecast = json['forecast'] != null
        ? ForecastModel.fromJson(json['forecast'])
        : null;
  }
  LocationModel? _location;
  CurrentModel? _current;
  ForecastModel? _forecast;
  WeatherModel copyWith({
    LocationModel? location,
    CurrentModel? current,
    ForecastModel? forecast,
  }) =>
      WeatherModel(
        location: location ?? _location,
        current: current ?? _current,
        forecast: forecast ?? _forecast,
      );
  LocationModel get location => _location ?? LocationModel();
  CurrentModel get current => _current ?? CurrentModel();
  ForecastModel get forecast => _forecast ?? ForecastModel();

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_location != null) {
      map['location'] = _location?.toJson();
    }
    if (_current != null) {
      map['current'] = _current?.toJson();
    }
    if (_forecast != null) {
      map['forecast'] = _forecast?.toJson();
    }
    return map;
  }

  Weather toEntity() {
    final now = DateTime.now();

    return Weather(
        currentWeather: current.toEntity(),
        locName: location.name,
        hourlyForecast: forecast.forecastday
            .firstWhere((element) {
              return DateTime.parse(element.date) ==
                  DateTime(now.year, now.month, now.day);
            })
            .hour
            .map((e) => e.toEntity())
            .toList(),
        dailyForecast: forecast.forecastday.map((e) => e.toEntity()).toList());
  }
}
