class ConditionModel {
  ConditionModel({
    String? text,
    String? icon,
    num? code,
  }) {
    _text = text;
    _icon = icon;
    _code = code;
  }

  ConditionModel.fromJson(dynamic json) {
    _text = json['text'];
    _icon = json['icon'];
    _code = json['code'];
  }
  String? _text;
  String? _icon;
  num? _code;
  ConditionModel copyWith({
    String? text,
    String? icon,
    num? code,
  }) =>
      ConditionModel(
        text: text ?? _text,
        icon: icon ?? _icon,
        code: code ?? _code,
      );
  String get text => _text ?? '';
  String get icon => _icon ?? '';
  num get code => _code ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['text'] = _text;
    map['icon'] = _icon;
    map['code'] = _code;
    return map;
  }
}
