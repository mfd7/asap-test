import 'package:asap_weather/data/weather/models/condition_model.dart';
import 'package:asap_weather/domain/weather/entities/current.dart';

class CurrentModel {
  CurrentModel({
    num? lastUpdatedEpoch,
    String? lastUpdated,
    num? tempC,
    num? tempF,
    num? isDay,
    ConditionModel? condition,
    num? windMph,
    num? windKph,
    num? windDegree,
    String? windDir,
    num? pressureMb,
    num? pressureIn,
    num? precipMm,
    num? precipIn,
    num? humidity,
    num? cloud,
    num? feelslikeC,
    num? feelslikeF,
    num? visKm,
    num? visMiles,
    num? uv,
    num? gustMph,
    num? gustKph,
  }) {
    _lastUpdatedEpoch = lastUpdatedEpoch;
    _lastUpdated = lastUpdated;
    _tempC = tempC;
    _tempF = tempF;
    _isDay = isDay;
    _condition = condition;
    _windMph = windMph;
    _windKph = windKph;
    _windDegree = windDegree;
    _windDir = windDir;
    _pressureMb = pressureMb;
    _pressureIn = pressureIn;
    _precipMm = precipMm;
    _precipIn = precipIn;
    _humidity = humidity;
    _cloud = cloud;
    _feelslikeC = feelslikeC;
    _feelslikeF = feelslikeF;
    _visKm = visKm;
    _visMiles = visMiles;
    _uv = uv;
    _gustMph = gustMph;
    _gustKph = gustKph;
  }

  CurrentModel.fromJson(dynamic json) {
    _lastUpdatedEpoch = json['last_updated_epoch'];
    _lastUpdated = json['last_updated'];
    _tempC = json['temp_c'];
    _tempF = json['temp_f'];
    _isDay = json['is_day'];
    _condition = json['condition'] != null
        ? ConditionModel.fromJson(json['condition'])
        : null;
    _windMph = json['wind_mph'];
    _windKph = json['wind_kph'];
    _windDegree = json['wind_degree'];
    _windDir = json['wind_dir'];
    _pressureMb = json['pressure_mb'];
    _pressureIn = json['pressure_in'];
    _precipMm = json['precip_mm'];
    _precipIn = json['precip_in'];
    _humidity = json['humidity'];
    _cloud = json['cloud'];
    _feelslikeC = json['feelslike_c'];
    _feelslikeF = json['feelslike_f'];
    _visKm = json['vis_km'];
    _visMiles = json['vis_miles'];
    _uv = json['uv'];
    _gustMph = json['gust_mph'];
    _gustKph = json['gust_kph'];
  }
  num? _lastUpdatedEpoch;
  String? _lastUpdated;
  num? _tempC;
  num? _tempF;
  num? _isDay;
  ConditionModel? _condition;
  num? _windMph;
  num? _windKph;
  num? _windDegree;
  String? _windDir;
  num? _pressureMb;
  num? _pressureIn;
  num? _precipMm;
  num? _precipIn;
  num? _humidity;
  num? _cloud;
  num? _feelslikeC;
  num? _feelslikeF;
  num? _visKm;
  num? _visMiles;
  num? _uv;
  num? _gustMph;
  num? _gustKph;
  CurrentModel copyWith({
    num? lastUpdatedEpoch,
    String? lastUpdated,
    num? tempC,
    num? tempF,
    num? isDay,
    ConditionModel? condition,
    num? windMph,
    num? windKph,
    num? windDegree,
    String? windDir,
    num? pressureMb,
    num? pressureIn,
    num? precipMm,
    num? precipIn,
    num? humidity,
    num? cloud,
    num? feelslikeC,
    num? feelslikeF,
    num? visKm,
    num? visMiles,
    num? uv,
    num? gustMph,
    num? gustKph,
  }) =>
      CurrentModel(
        lastUpdatedEpoch: lastUpdatedEpoch ?? _lastUpdatedEpoch,
        lastUpdated: lastUpdated ?? _lastUpdated,
        tempC: tempC ?? _tempC,
        tempF: tempF ?? _tempF,
        isDay: isDay ?? _isDay,
        condition: condition ?? _condition,
        windMph: windMph ?? _windMph,
        windKph: windKph ?? _windKph,
        windDegree: windDegree ?? _windDegree,
        windDir: windDir ?? _windDir,
        pressureMb: pressureMb ?? _pressureMb,
        pressureIn: pressureIn ?? _pressureIn,
        precipMm: precipMm ?? _precipMm,
        precipIn: precipIn ?? _precipIn,
        humidity: humidity ?? _humidity,
        cloud: cloud ?? _cloud,
        feelslikeC: feelslikeC ?? _feelslikeC,
        feelslikeF: feelslikeF ?? _feelslikeF,
        visKm: visKm ?? _visKm,
        visMiles: visMiles ?? _visMiles,
        uv: uv ?? _uv,
        gustMph: gustMph ?? _gustMph,
        gustKph: gustKph ?? _gustKph,
      );
  num get lastUpdatedEpoch => _lastUpdatedEpoch ?? 0;
  String get lastUpdated => _lastUpdated ?? '';
  num get tempC => _tempC ?? 0;
  num get tempF => _tempF ?? 0;
  num get isDay => _isDay ?? 0;
  ConditionModel get condition => _condition ?? ConditionModel();
  num get windMph => _windMph ?? 0;
  num get windKph => _windKph ?? 0;
  num get windDegree => _windDegree ?? 0;
  String get windDir => _windDir ?? '';
  num get pressureMb => _pressureMb ?? 0;
  num get pressureIn => _pressureIn ?? 0;
  num get precipMm => _precipMm ?? 0;
  num get precipIn => _precipIn ?? 0;
  num get humidity => _humidity ?? 0;
  num get cloud => _cloud ?? 0;
  num get feelslikeC => _feelslikeC ?? 0;
  num get feelslikeF => _feelslikeF ?? 0;
  num get visKm => _visKm ?? 0;
  num get visMiles => _visMiles ?? 0;
  num get uv => _uv ?? 0;
  num get gustMph => _gustMph ?? 0;
  num get gustKph => _gustKph ?? 0;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['last_updated_epoch'] = _lastUpdatedEpoch;
    map['last_updated'] = _lastUpdated;
    map['temp_c'] = _tempC;
    map['temp_f'] = _tempF;
    map['is_day'] = _isDay;
    if (_condition != null) {
      map['condition'] = _condition?.toJson();
    }
    map['wind_mph'] = _windMph;
    map['wind_kph'] = _windKph;
    map['wind_degree'] = _windDegree;
    map['wind_dir'] = _windDir;
    map['pressure_mb'] = _pressureMb;
    map['pressure_in'] = _pressureIn;
    map['precip_mm'] = _precipMm;
    map['precip_in'] = _precipIn;
    map['humidity'] = _humidity;
    map['cloud'] = _cloud;
    map['feelslike_c'] = _feelslikeC;
    map['feelslike_f'] = _feelslikeF;
    map['vis_km'] = _visKm;
    map['vis_miles'] = _visMiles;
    map['uv'] = _uv;
    map['gust_mph'] = _gustMph;
    map['gust_kph'] = _gustKph;
    return map;
  }

  Current toEntity() {
    return Current(
      tempC: tempC,
      tempF: tempF,
      status: condition.text,
      feelsLikeC: feelslikeC,
      feelsLikeF: feelslikeF,
      humidity: humidity,
      pressureBar: pressureMb,
      pressurePsi: pressureIn,
      windSpeedKph: windKph,
      windSpeedMph: windMph,
      cloud: cloud,
      iconCode: condition.code,
    );
  }
}
