import 'package:asap_weather/data/weather/models/astro_model.dart';
import 'package:asap_weather/data/weather/models/day_model.dart';
import 'package:asap_weather/data/weather/models/hour_model.dart';
import 'package:asap_weather/domain/weather/entities/daily_forecast.dart';
import 'package:asap_weather/domain/weather/entities/hourly_forecast.dart';

class ForecastDayModel {
  ForecastDayModel({
    String? date,
    num? dateEpoch,
    DayModel? day,
    AstroModel? astro,
    List<HourModel>? hour,
  }) {
    _date = date;
    _dateEpoch = dateEpoch;
    _day = day;
    _astro = astro;
    _hour = hour;
  }

  ForecastDayModel.fromJson(dynamic json) {
    _date = json['date'];
    _dateEpoch = json['date_epoch'];
    _day = json['day'] != null ? DayModel.fromJson(json['day']) : null;
    _astro = json['astro'] != null ? AstroModel.fromJson(json['astro']) : null;
    if (json['hour'] != null) {
      _hour = [];
      json['hour'].forEach((v) {
        _hour?.add(HourModel.fromJson(v));
      });
    }
  }
  String? _date;
  num? _dateEpoch;
  DayModel? _day;
  AstroModel? _astro;
  List<HourModel>? _hour;
  ForecastDayModel copyWith({
    String? date,
    num? dateEpoch,
    DayModel? day,
    AstroModel? astro,
    List<HourModel>? hour,
  }) =>
      ForecastDayModel(
        date: date ?? _date,
        dateEpoch: dateEpoch ?? _dateEpoch,
        day: day ?? _day,
        astro: astro ?? _astro,
        hour: hour ?? _hour,
      );
  String get date => _date ?? '';
  num? get dateEpoch => _dateEpoch;
  DayModel get day => _day ?? DayModel();
  AstroModel? get astro => _astro;
  List<HourModel> get hour => _hour ?? [];

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['date'] = _date;
    map['date_epoch'] = _dateEpoch;
    if (_day != null) {
      map['day'] = _day?.toJson();
    }
    if (_astro != null) {
      map['astro'] = _astro?.toJson();
    }
    if (_hour != null) {
      map['hour'] = _hour?.map((v) => v.toJson()).toList();
    }
    return map;
  }

  DailyForecast toEntity() {
    return DailyForecast(
      dateTime: DateTime.parse(date),
      humidity: day.avghumidity,
      icon: day.condition.icon,
      maxTempC: day.maxtempC,
      minTempC: day.mintempC,
      maxTempF: day.maxtempF,
      minTempF: day.mintempF,
    );
  }
}
