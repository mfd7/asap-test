import 'package:asap_weather/data/weather/models/forecast_day_model.dart';

class ForecastModel {
  ForecastModel({
    List<ForecastDayModel>? forecastday,
  }) {
    _forecastday = forecastday;
  }

  ForecastModel.fromJson(dynamic json) {
    if (json['forecastday'] != null) {
      _forecastday = [];
      json['forecastday'].forEach((v) {
        _forecastday?.add(ForecastDayModel.fromJson(v));
      });
    }
  }
  List<ForecastDayModel>? _forecastday;
  ForecastModel copyWith({
    List<ForecastDayModel>? forecastday,
  }) =>
      ForecastModel(
        forecastday: forecastday ?? _forecastday,
      );
  List<ForecastDayModel> get forecastday => _forecastday ?? [];

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_forecastday != null) {
      map['forecastday'] = _forecastday?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}
