import 'package:asap_weather/data/_core/api_base_helper.dart';
import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/weather/models/weather_model.dart';
import 'package:asap_weather/data/weather/weather_endpoints.dart';

abstract class WeatherRemoteDatasource {
  Future<WeatherModel> retrieveWeather({String? queryId});
}

class WeatherRemoteDatasourceImpl implements WeatherRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  WeatherRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<WeatherModel> retrieveWeather({String? queryId}) async {
    var query = {'days': '7'};
    if (queryId != null) {
      query['q'] = 'id:$queryId';
    }
    final response = await apiBaseHelper
        .getApi(WeatherEndpoints.currentAndForecast, qParams: query);
    try {
      return WeatherModel.fromJson(response);
    } catch (_) {
      throw ParseDataException();
    }
  }
}
