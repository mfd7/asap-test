import 'package:asap_weather/data/_core/exception.dart';
import 'package:asap_weather/data/_core/failure.dart';
import 'package:asap_weather/data/weather/datasources/weather_remote_datasource.dart';
import 'package:asap_weather/domain/weather/entities/weather.dart';
import 'package:asap_weather/domain/weather/repositories/weather_repository.dart';
import 'package:dartz/dartz.dart';

class WeatherRepositoryImpl implements WeatherRepository {
  final WeatherRemoteDatasource weatherRemoteDatasource;

  WeatherRepositoryImpl(this.weatherRemoteDatasource);
  @override
  Future<Either<Failure, Weather>> retrieveWeather({String? queryId}) async {
    try {
      final responses =
          await weatherRemoteDatasource.retrieveWeather(queryId: queryId);
      return Right(responses.toEntity());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }
}
